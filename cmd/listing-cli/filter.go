package main

import (
	"log"

	"gitlab.com/ribtoks/listing/pkg/common"
)

func (c *listingClient) addExcludedSubscribers(ss []*common.SubscriberEx) {
	if len(ss) == 0 {
		return
	}

	for _, s := range ss {
		c.excludedSubscribers[s.Email] = true
	}
}

func (c *listingClient) filter(data []byte) error {
	ss, err := c.parseSubscribers(data)
	if err != nil {
		return err
	}

	skipped := 0
	for _, s := range ss {
		if c.isSubscriberOK(s) {
			c.printer.Append(s)
		} else {
			skipped++
		}
	}
	c.printer.Render()
	log.Printf("Filtered subscribers. count=%v skipped=%v", len(ss), skipped)
	return nil
}
