package main

import (
	"bytes"
	"encoding/json"
	"errors"
	"flag"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"os"
	"time"

	"gitlab.com/ribtoks/listing/pkg/common"
)

var (
	modeFlag             = flag.String("mode", "", "Execution mode: subscribe|unsubscribe|export|import|delete")
	urlFlag              = flag.String("url", "", "Base URL to the listing API")
	emailFlag            = flag.String("email", "", "Email for subscribe|unsubscribe")
	authTokenFlag        = flag.String("auth-token", "", "Auth token for admin access")
	secretFlag           = flag.String("secret", "", "Secret for email salt")
	newsletterFlag       = flag.String("newsletter", "", "Newsletter for subscribe|unsubscribe")
	formatFlag           = flag.String("format", "table", "Output format of subscribers: csv|tsv|table|raw|yaml")
	nameFlag             = flag.String("name", "", "(optional) Name for subscribe")
	logPathFlag          = flag.String("l", "listing-cli.log", "Absolute path to log file")
	stdoutFlag           = flag.Bool("stdout", false, "Log to stdout and to logfile")
	helpFlag             = flag.Bool("help", false, "Print help")
	dryRunFlag           = flag.Bool("dry-run", false, "Simulate selected action")
	noUnconfirmedFlag    = flag.Bool("no-unconfirmed", false, "Do not export unconfirmed emails")
	noConfirmedFlag      = flag.Bool("no-confirmed", false, "Do not export confirmed emails")
	noUnsubscribedFlag   = flag.Bool("no-unsubscribed", false, "Do not export unsubscribed emails")
	ignoreComplaintsFlag = flag.Bool("ignore-complaints", false, "Ignore bounces and complaints for export")
	excludeFileFlag      = flag.String("exclude-file", "", "File with subscribers to ignore")
)

const (
	appName         = "listing-cli"
	modeSubscribe   = "subscribe"
	modeUnsubscribe = "unsubscribe"
	modeExport      = "export"
	modeImport      = "import"
	modeDelete      = "delete"
	modeFilter      = "filter"
)

func main() {
	err := parseFlags()
	if err != nil {
		flag.PrintDefaults()
		log.Fatal(err.Error())
	}

	logfile, err := setupLogging()
	if err != nil {
		defer logfile.Close()
	}

	client := &listingClient{
		client: &http.Client{
			Timeout: 10 * time.Second,
			CheckRedirect: func(req *http.Request, via []*http.Request) error {
				return http.ErrUseLastResponse
			},
		},
		printer:             NewPrinter(),
		url:                 *urlFlag,
		authToken:           *authTokenFlag,
		secret:              *secretFlag,
		complaints:          make(map[string]bool),
		excludedSubscribers: make(map[string]bool),
		dryRun:              *dryRunFlag,
		noUnconfirmed:       *noUnconfirmedFlag,
		noConfirmed:         *noConfirmedFlag,
		noUnsubscribed:      *noUnsubscribedFlag,
		ignoreComplaints:    *ignoreComplaintsFlag,
		excludeFile:         *excludeFileFlag,
	}

	switch *modeFlag {
	case modeExport:
		{
			err = client.export(*newsletterFlag)
		}
	case modeFilter:
		{
			if ss, err := readExcludedSubscribers(*excludeFileFlag); err == nil {
				client.addExcludedSubscribers(ss)
			}

			bytes, _ := ioutil.ReadAll(os.Stdin)
			err = client.filter(bytes)
		}
	case modeSubscribe:
		{
			err = client.subscribe(*emailFlag, *newsletterFlag, *nameFlag)
		}
	case modeUnsubscribe:
		{
			err = client.unsubscribe(*emailFlag, *newsletterFlag)
		}
	case modeImport:
		{
			bytes, _ := ioutil.ReadAll(os.Stdin)
			err = client.importSubscribers(bytes)
		}
	case modeDelete:
		{
			bytes, _ := ioutil.ReadAll(os.Stdin)
			err = client.deleteSubscribers(bytes)
		}
	default:
		fmt.Printf("Mode %v is not supported yet", *modeFlag)
	}
	if err != nil {
		fmt.Printf("Error: %v", err)
		os.Exit(1)
	}
}

// with format=json we save SubscriberEx to the output so we must read them as well
func readExcludedSubscribers(filepath string) ([]*common.SubscriberEx, error) {
	if len(filepath) == 0 {
		return nil, errors.New("exclude filepath is empty")
	}

	f, err := os.Open(filepath)
	if err != nil {
		return nil, err
	}
	defer f.Close()

	data, err := ioutil.ReadAll(f)
	if err != nil {
		return nil, err
	}

	dec := json.NewDecoder(bytes.NewBuffer(data))
	dec.DisallowUnknownFields()

	var subscribers []*common.SubscriberEx
	err = dec.Decode(&subscribers)
	if err != nil {
		return nil, err
	}
	log.Printf("Parsed excluded subscribers. count=%v", len(subscribers))

	return subscribers, nil
}

func setupLogging() (f *os.File, err error) {
	f, err = os.OpenFile(*logPathFlag, os.O_RDWR|os.O_CREATE|os.O_APPEND, 0666)
	if err != nil {
		fmt.Printf("error opening file: %v", *logPathFlag)
		return nil, err
	}

	if *stdoutFlag || *dryRunFlag {
		mw := io.MultiWriter(os.Stdout, f)
		log.SetOutput(mw)
	} else {
		log.SetOutput(f)
	}

	log.Println("------------------------------")
	log.Println(appName + " log started")

	return f, err
}

func parseFlags() error {
	flag.Parse()

	switch *modeFlag {
	case "":
		return errors.New("mode is required")
	case modeDelete, modeExport, modeImport, modeSubscribe, modeUnsubscribe, modeFilter:
		break
	default:
		return fmt.Errorf("mode %v is not supported", *modeFlag)
	}

	if *modeFlag != modeFilter {
		switch *urlFlag {
		case "":
			return errors.New("url is required")
		default:
			if _, e := url.Parse(*urlFlag); e != nil {
				return fmt.Errorf("failed to parse url. err=%v", e)
			}
		}
	}

	switch *modeFlag {
	case modeExport, modeUnsubscribe, modeFilter:
		if *secretFlag == "" {
			return errors.New("secret flag is required")
		}
	}

	switch *modeFlag {
	case modeExport, modeImport, modeDelete:
		if *authTokenFlag == "" {
			return errors.New("auth token is required")
		}
	}

	return nil
}

// NewPrinter creates a new printer
func NewPrinter() Printer {
	switch *formatFlag {
	case "table":
		return NewTablePrinter(*secretFlag)
	case "csv":
		return NewCSVPrinter(*secretFlag)
	case "tsv":
		return NewTSVPrinter(*secretFlag)
	case "raw":
		return NewRawPrinter()
	case "json":
		return NewJsonPrinter(*secretFlag)
	case "yaml":
		return NewYamlPrinter(*secretFlag)
	default:
		return NewTablePrinter(*secretFlag)
	}
}
