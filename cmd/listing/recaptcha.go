package main

import (
	"encoding/json"
	"errors"
	"io/ioutil"
	"log"
	"net/http"
	"net/url"
	"strconv"
	"strings"
)

type GoogleRecaptcha struct {
	secret string
}

var (
	errRecaptchaFailed = errors.New("captcha did not pass")
)

const (
	paramSecret   = "secret"
	paramResponse = "response"
	recaptchaURL  = "https://www.google.com/recaptcha/api/siteverify"
)

type recaptchaResponse struct {
	Success bool `json:"success"`
	Others  json.RawMessage
}

func (gr *GoogleRecaptcha) VerifyResponse(response string) error {
	if len(gr.secret) == 0 {
		return nil
	}

	log.Printf("Verifying captcha. response=%v", response)

	data := url.Values{}
	data.Set(paramSecret, gr.secret)
	data.Set(paramResponse, response)

	encoded := data.Encode()

	req, err := http.NewRequest("POST", recaptchaURL, strings.NewReader(encoded))
	if err != nil {
		return err
	}

	req.Header.Add("Content-Type", "application/x-www-form-urlencoded")
	req.Header.Add("Content-Length", strconv.Itoa(len(encoded)))

	client := &http.Client{}
	resp, err := client.Do(req)

	if err != nil {
		return err
	}
	defer resp.Body.Close()

	body, err := ioutil.ReadAll(resp.Body)
	if err != nil {
		return err
	}

	captchaResponse := &recaptchaResponse{}
	if err := json.Unmarshal(body, &captchaResponse); err != nil {
		return err
	}

	if !captchaResponse.Success {
		log.Printf("Captcha response failed. response=%v", string(body))
		return errRecaptchaFailed
	}

	return nil
}
