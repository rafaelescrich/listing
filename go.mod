module gitlab.com/ribtoks/listing

go 1.14

require (
	github.com/aws/aws-lambda-go v1.19.1
	github.com/aws/aws-sdk-go v1.34.13
	github.com/awslabs/aws-lambda-go-api-proxy v0.8.0
	github.com/badoux/checkmail v1.2.0 // indirect
	github.com/go-gomail/gomail v0.0.0-20160411212932-81ebce5c23df
	github.com/jpillora/backoff v1.0.0
	github.com/mattn/go-runewidth v0.0.9 // indirect
	github.com/olekukonko/tablewriter v0.0.4
	github.com/onsi/ginkgo v1.14.0 // indirect
	github.com/ribtoks/checkmail v0.0.0-20181210160741-9661bd69e9ad
	github.com/rs/xid v1.2.1
	gopkg.in/alexcesaro/quotedprintable.v3 v3.0.0-20150716171945-2caba252f4dc // indirect
	gopkg.in/gomail.v2 v2.0.0-20160411212932-81ebce5c23df // indirect
	gopkg.in/yaml.v2 v2.3.0
)
